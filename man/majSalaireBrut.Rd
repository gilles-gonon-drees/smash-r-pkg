% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/fonctions_salaire.R
\name{majSalaireBrut}
\alias{majSalaireBrut}
\title{Évolution linéaire du salaire brut}
\usage{
majSalaireBrut(df, calTime, pente_evolution = 12 * 30.65)
}
\arguments{
\item{df}{: data.frame données de la population à l'instant t. Doit contenir
la variable S_BRUT.}

\item{calTime}{: année ou date calendaire}

\item{pente_evolution}{: évolution du salaire, liée à calTime (typiquement
évolution annuelle). La valeur par défaut est l'évolution moyenne du brut de
l'ensemble des échelons (voir notebook : Tests/analyse_echelon.Rmd)}
}
\value{
df avec S_BRUT mis à jour.
}
\description{
Cette fonction incrémente l'ensemble Des salaires de la FPH d'une constante.
Elle permet de simuler une augmentation moyenne de la masse salariale
globale, et constitue l'approximation la plus grossière du modèle.
}
\examples{
df <- data.frame(AGE = c(25,25,70),
                 STATUT = c("FPH", "FPH", "Decede"),
                 S_BRUT = c(1000, 2000, 3000) )
df <- majSalaireBrut(df, 2021)
}
