# Simulation d'une carrière pour un grade ----
# - Population initiale d'entrants SIASP selon modèle
# - Vieillissement : majAge + ancienneté FPH
# - Sorties : majMort + majRetraite
# - Evolutions : Indice / grade-métier / nomenclature
# - Pas d'entrants
# L'objectif est de voir les évolutions sur une carrière
# Sur un horizon court ce script permet ussi de voir les changements de
# nomenclature (typiquement < 10 ans)

# Pour faire tourner le script, copier la commande suivante dans la console :
# source("demo/simul_carriere_grade.R", encoding = "utf-8", echo=FALSE)
devtools::load_all()

## Paramètres ----------

# Horizon de simulation :
# Sur un horizon court ce script permet de voir les changements de nomenclature
anneeDebut <- 2016
anneeFin <-  anneeDebut + 45

N <- 10000 # Nombre de personnes simulées
l_grades_simu <- c("3001") # Grade des entrants simulés en population initiale

## Chemins modèles ----
path_modeles = "~/data/modeles" # "T:/BPS/SMASH/modeles" #

model_entrants <- file.path(path_modeles, "modele_entrants_v3_hop_2014-2016.rds")
model_entrants_anplus1 <- file.path(path_modeles, "modele_entrants_hop_anplus1_2014-2016.rds")
model_retraite  <- file.path(path_modeles, "mdl_retraites_taux_hop_2016.rds")
model_mortalite  <- file.path(path_modeles, "mortalite_Ensemble.rds")
modele_transitionGrade <- file.path(path_modeles, "modele_transitionGrade_TableProbabilite.rds")

## Choix des méthodes de calcul (Fonctions) ----
# Choisir/écrire les fonctions à appliquer

### Fonctions de calcul des indicateurs résultats ----
lFcnsCalculsResultats <- c(calculCoutEmployeur)

### Liste des fonctions de mises à jour ----
lFcnsMaJ <- c(
  resetISSortant,
  majAge,
  majAncienneteFPH,
  function(df, calTime) majMort(df, calTime, fic_mortalite = model_mortalite),
  function(df, calTime) majRetraitesTaux(df, calTime, fic_retraite = model_retraite),
  function(df, calTime) majIndiceMajore(df, calTime),
  function(df, calTime) majNomenclatureGrade(df, calTime, majsbrut = TRUE),
  function(df, calTime) majEvolutionGrade(df, calTime,fic_model = modele_transitionGrade),
  function(df, calTime) calculChargesEmployeurTx(df, calTime, taux = 0.46)
  )

#### Fonctions de visualisations
# 1: fonctions appliquées à df_resultats : résultats précalculés dans la simulation
lFcnsVisus <- c(plot_bar_effectifs)

# 2: fonctions appliquées à popSimul
lFcnsVisusPopSimul <- c(
  plot_pop_hist_age,
  plot_bar_eqtp_statut,
  plot_bar_massesalariale_statut
  )

if ((anneeFin - anneeDebut)<10) {
  # Si l'horizon est court on regarde les flux de grade
  lFcnsVisusPopSimul <- c(lFcnsVisusPopSimul, plot_flux_grade)
}

# 3021 : l_grades = c("3021", "3022", "2154","2164")
# 2154 : l_grades = c("2154","2164","1801","2962","2948")

## Création de la population initiale ------
# On supprime les contrats courts
popInit <- genererEntrantsStatut(N, anneeDebut,
                                 l_statuts = c("TITH"),
                                 l_grades = l_grades_simu,
                                 fic_model = model_entrants
                                 ) %>% filter(IS_SORTANT == FALSE)

popInit <- majEQTPEntrantsTx(popInit, anneDebut, model_entrants_anplus1)
popInit$IS_ENTRANT <- F
popInit <- calculChargesEmployeurTx(popInit, anneeDebut, taux = 0.46)

## Lancement de la simulation ----
l_pop_simul <- lancer_simulation(
  popInit = popInit,
  anneeDebut = anneeDebut,
  anneeFin = anneeFin,
  lFcnsCalculs = lFcnsCalculsResultats,
  lFcnsMaJ = lFcnsMaJ,
  list_corps = list_corps
  )

## Résultats et visualisations ----
df_resultat <- calculer_indicateurs(
  l_pop_simul,
  lFcnsVisus = lFcnsVisus,
  lFcnsVisusPopSimul = lFcnsVisusPopSimul
  )

