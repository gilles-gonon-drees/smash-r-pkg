---
title: "Prise en main"
author: "BPS/OSAM/DREES - Équipe SMASH"
date: "`r format(Sys.time(), '%d %B, %Y')`"
description: >
  Découverte et prise en main de l'outil de micro-simulation SMASH.
output: rmarkdown::html_vignette
vignette: >
  %\VignetteIndexEntry{Prise en main}
  %\VignetteEngine{knitr::rmarkdown}
  %\VignetteEncoding{UTF-8}
---


# Projet SMASH - langage R <img src="images/logo-150.png" align="right" width="12%;"/>


## Introduction 

SMASH est un outil de micro-**S**imulation de la **MA**sse **S**alariale
**H**ospitalière développé à la DREES par le bureau des Professions de Santé
avec le soutien du Lab Santé.

Ce dépôt rassemble les premiers tests de développement et briques de 
micro-simulation. 

> Le projet est dans un état de développement alpha. Ce dépot contient la
mise en package du code de micro-simulation qui restera en phase de développement
intensif jusqu'en décembre 2022.

Reporter vous à la section [**Présentation**](articles/index.html) pour une
description plus détaillée du package. 

## Démarrage rapide

### Installation

Le package est en phase de développement intensif, avec une première version 
fonctionnelle attendue pour juillet 2022. L'installation se fait depuis les 
sources du package sur Gitlab. 

```r
remotes::install_gitlab('gilles-gonon-drees/smash-r-pkg')
```

### Exemples des démonstrations

Des exemples de script pour lancer et paramétrer une micro-simulation sont 
fournis dans le dossier `demo`. Typiquement : 

```r 
source("demo/simul_equations_salaire.R", encoding = "utf-8", echo=FALSE)
```

*Remarques :*

- Les modèles ne sont pas encore inclus dans le dépôt, ce qui limite actuellement
la possibilité de lancer des simulations aux personnes de la DREES. Désolés, 
nous travaillons à intégrer les modèles dans les prochaines versions. 
- Il faut avoir accès au disque T de la DREES pour lancer la simulation 
(données & modèle)
- Vous pouvez choisir le chemin local si vos modèles sont en locaux


## Création de la base de données SMASH

L'ensemble des informations relatives à la nomenclature des emplois hospitaliers
et les grilles associées
La base de données SMASH est générée à partir de 3 fichiers Excel disponibles 
sur le disque T de la DREES. Ce sont des fichiers de travail qui évoluent 
régulièrement : 

- `nehpnm2020.xls` : Nomemclature des emplois hospitaliers, personnel non médical
- `20211217_neh_grilles_sas.xlsx` : Liens entre grade et grilles et définition 
des grilles
- `Tables_Reclassements.xlsx` : Règles de reclassement des grilles de rémunération

Pour recréer la base de données, il faut lancer le script `data-raw/CreationBDDGrillesFromExcel.R`.

Un lot de tests unitaires permet de détecter un certain nombre d'anomalies sur 
les informations saisies. Lisez bien les messages et warnings issus des tests 
lors la création de la BDD pour savoir quel grade/grille corriger.

L'ensemble des tests est contenu et commenté dans le fichier
`testthat/test-GrillesIndiciaires.R`

La manière la plus simple de relancer les tests sur la BDD est : 

```r
devtools::load_all(".")
testthat::test_file(file.path(test_path(), "test-GrillesIndiciaires.R"))
```
