---
editor_options: 
  markdown: 
    wrap: 72
---

# Projet SMASH - langage R

## Avant-propos

Le package SMASH est en phase de développement intensif et évolue quasiment
quotidiennement. 

Nous vous recommandons d'installer le package à partir du dépôt Git du code 
source. 


## Introduction

SMASH est un outil de micro-**S**imulation de la **MA**sse **S**alariale
**H**ospitalière développé à la DREES par le bureau des Professions de
Santé avec le soutien du Lab Santé.

Ce dépôt rassemble les premiers tests de développement et briques de
micro-simulation.

> Le projet est dans un état de développement **beta**. Ce dépot contient
> la mise en package du code de micro-simulation et reste en phase de
> développement intensif.

## Démarrage rapide

Différents exemples de scripts pour lancer et paramétrer des micro-simulations
sont fournis dans le dossier `demo`. 

Vous pouvez lancer un exemple sur des données de population synthétiques 
pour voir le type de projections réalisées par SMASH : 

``` r
source(system.file("exemples", "demo_smash_fakePop.R", package = "smash"), 
        encoding = "utf-8", echo=FALSE)
```

Si vous être à la DREES et avez accès au données SIASP, vous pouvez lancer 
directement un exemple de projection utilisé pour les projections ONDAM :

``` r
source(system.file("exemples", "simul_scenario_ondam.R", package = "smash"), 
        encoding = "utf-8", echo=FALSE)
```

*Remarques :*

-   Il est possible de générer une population initiale synthétique, fonction `genererFakePopInit`
-   Il faut avoir accès au disque `T` de la DREES pour lancer la
    simulation (données & modèle)
-   Vous pouvez choisir le chemin local si vos modèles sont en locaux


## Développer le package en local

### Organisation des fonctions

Les fonctions sont réparties dans des fichiers dont les noms tentent
d'être aussi explicites que possible.

-   **fonctions_entrants.R** : génération et mise à jour d'entrants FPH
-   **fonctions_evolutions.R** : fonction diverses d'évolution de la
    population
-   **fonctions_initialisation.R** : génération la population initiale
    du simulateur
-   **fonctions_salaire.R** : calcul des salaires et primes pour les
    différents statuts
-   **fonctions_simulations.R** : lancement de la simulation principale,
    calcul des résultats et
-   **fonctions_sortants.R** : sélection et mise à jour des sortants FPH
-   **fonctions_visualisation.R** : visualisation
-   **GrillesIndiciaires.R** : Classe d'accès aux données réglementaires
    (grilles, PASS, point d'indice, ...)
-   **Reformes.R** : Classe pour modifier le cadre réglementaire,
    ajouter des grilles, ...
-   **utils.R** : fonctions utilitaires du package
-   **classes_utils.R** : classes utilitaires de traitement des données

### Environnement

Pour *développer* le package en local, les packages suivants sont
nécessaires :

-   `devtools`
-   `usethis`
-   `testthat`

La **fonction** `load_all()` charge le package en local et permet
d'accéder aux fonctions comme avec `library`.

``` r
devtools::load_all()
```

Après avoir effectué des modifications sur le code, il sera plus
intéressant d'utiliser la commande `document()` qui mettra la
documentation à jour et rafraîchira les modifications (plus long).

``` r
devtools::document()
```

Il est également recommandé d'utiliser `renv` pour la gestion des librairies. 
Pour cela installer le package `renv` puis initialiser l'environnement virtuel
avec la commande :

```r
renv::init()
```

### Codage et tests des fonctions

#### Mémo pour l'ajout d'une variable

1.  Ajouter la variable et son affectation
2.  Enlever les populations initiales existantes dans les dossiers
    racine du package et dossier `~/data/temp`, fichiers de type
    `popInit_2016_0.rds`
3.  Fichier `fonctions_entrants` : les variables ajoutées doivent être
    présentes dans les populations d'entrants générées : cela implique a
    minima de renseigner une valeur par défaut et idéalement de
    réapprendre les modèles avec cette variable.
4.  Faire les modifications sur les fonctions impactées et
    visualisations, ...

### Tests de micro-simulations

-   Pour le développement de nouvelles fonctionnalités, par exemple une
    nouvelle fonction de mise à jour, le plus simple est de partir d'une
    copie d'un fichier `simul_...` du dossier `demo` pour lancer vos
    tests, et d'ajouter votre fonction dans le fichier R pertinent
    (*eg.* `R/utils_fonctions.R`).

-   Si vos modifications impactent les fonctions existantes, il sera
    plus prudent créer une branche GIT pour réaliser vos modifications
    avant de les intégrer.

-   Création de la branche

``` bash
git checkout -b ma_fonction
git push -u origin ma_fonction
```

-   Intégration des modifications faites dans la branche vers main

``` bash
git checkout main
git merge ma_fonction
```

### Vérification des simulations après modification

Voici un exemple de code R pour lancer l'ensemble des simulations du
dossier `exemples` sous forme de *local job*.

``` r
chemin_ex <- system.file("exemples", package = "smash")
rSimuls <- list.files(chemin_ex, "*.R")
for (s in rSimuls) {
   rstudioapi::jobRunScript(file.path(chemin_ex, s), encoding = "UTF-8", 
                          workingDir = NULL, importEnv = FALSE, exportEnv = "")
}
```


### Tests unitaires

Les tests unitaires sont réalisés à partir du package `testthat`. Voir
<https://testthat.r-lib.org/> . Pour lancer les tests du package :

``` r
devtools::test()
```

Il également possible de voir la couverture de code en terme de tests
unitaires avec le package `covr`.

``` r
covr::package_coverage()
```

### Documentation

La documentation est écrite dans le code des fonctions avec la syntaxe
roxygen. Les pages supplémentaires sont dans le dossier `vignettes`.

-   Pour générer l'aide R pour le package, utiliser la commande :
    `devtools::document()`
-   Pour générer une aide HTML au format plus confortable :
    `pkgdown::build_site()` ou `pkgdown::build_reference()` (voir la
    [documentation pkgdown](https://pkgdown.r-lib.org/))

*Remarque* : si le build de la documentation HTML échoue à cause d'une
erreur de type `curl::...` (du au proxy), il faut utiliser l'option sans
connexion internet.

``` r
options(pkgdown.internet = FALSE)
devtools::document()
pkgdown::build_site(devel = TRUE)
```

#### Publier la documentation en ligne

Le fichier .gitlab-ci.yml génère une page web de la documentation
lorsqu'on `push` sur la branche `publish` du dépôt.

Les commandes git pour déclencher la publication de la documentation
sont données ci-dessous, à lancer dans un terminal (celui de Rstudio par
exemple).

``` bash
git checkout publish
git merge main
git push
git checkout main
```

Les étapes sont :

-   Passer sur la branche `publish`
-   Récupérer le contenu de la branche `main` (merge)
-   `push` : envoie les modifs sur le dépot en ligne
-   Retour à la branche `main`

### Vérification, compilation, installation du package

-   Pour valider le package : `devtools::check()`,
-   Pour compiler en .zip : `devtools::build()`

Lors du développement local on pourra lancer plutôt la commande suivante pour 
récupérer le log complet de la validation du package (parfois long) :

``` r
sink(file = "~/check.log", split = TRUE)
devtools::check(cran = FALSE, run_dont_test = FALSE, args = "--no-multiarch")
sink(NULL)
```

Enfin il est possible d'installer le package dans R, commande
`install()` mais ceci n'est pas recommandé car le package est
actuellement en phase de développement intensif.

### Créer une fonction

La création d'une nouvelle fonction peut être réalisée à l'aide de la
fonction utilitaire `use_r()`. Comme suggéré à la création de la
fonction, il est bon de créer dans la foulée la fonction de test
associée.

``` r
use_r("calculSalaires")
```

Pour créer les tests unitaires associés à la thématique on utilisera
`usethis` :

``` r
usethis::use_test("calculSalaires")
```

**Il n'est pas nécessaire que les noms soient identiques**.  
Notamment si le fichier `calculSalaires` contient différentes fonctions,
les tests peuvent porter sur une fonction précise du fichier.

## Données utilisées

-   Les données utilisées sont situées sur le disque T de la DREES
-   **Les données ne doivent pas être incluses dans ce dépôt**.
-   Les données pouvant être rendues disponibles (grilles indiciaires,
    infos réglementaires, modèles issus des données, ...) seront
    étudiées.
