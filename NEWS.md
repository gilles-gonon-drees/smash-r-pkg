---
editor_options: 
  markdown: 
    wrap: 80
---

# Avant-propos

Le package SMASH est en phase de développement intensif et évolue quasiment
quotidiennement. 

- 26/05/2023 : Le suivi indiciaire des titulaires avec SMASH est bon et intègre 
les évolutions de réglementation. Le calcul du GVT est réalisable pour les champs
HOP, ESMS et FPH.


# 🌾 SMASH 0.8.0

Corrections de bugs & réorganisation du package :

- Validation des modules d'évolution d'indice, reclassements, et promotion de 
grades : on suit les évolutions des individus avec une précision inférieure à 1%
- Déplacement des démos -> `inst/exemples`
- Déplacement des modèles de générations de rapports Rmd vers `inst/Rmd_modeles`
- Mise à jour des modèles d'EQTP des entrants N en N+1 en excluant les entrants 
N sortants en N+1 (contrats courts à cheval sur 2 ans) pour ne pas réduire 2 fois 
leur EQTP comme entrants N->N+1 et sortants N+1.
- Mise à jour des fonctions GVT pour la note GVT 2023, champs hopitaux ("hop"), 
ESMS ("esms") ou FPH ("fph").

# 🌿 SMASH 0.7.0

De nombreuses mises à jour des modèles. Dans l'objectif d'arriver rapidement 
à une version 1.0 du simulateur.

- Première version du module de contractualisation / CDIsation
- Mise à jour du modèle d'entrants pour générer des contrats courts (entrant et 
sortant la même année) et stables (reste au moins 2 année dans la FPH)
- Homogénéisation des statuts de contrats
- Modification de la BDD 
  - Renommage -> `BDD_SMASH.sqlite`
  - Ajout des reclassements
  - Législation 2023

# 🌱 SMASH 0.6.1 / 0.6.2

Ces versions ne présentent pas de nouveautés majeures mais elles incluent
les fonctions utilisées pour le calcul du GVT.

# 🧒 🍼 SMASH 0.6.0

## Changement généraux

- Les fichiers de population initiale sont maintenant sauvés au format fst
- Nettoyage et mise à jour des fichiers de demo/test des cas de simulation.
- Ajout des noms / codes régions à la BDD


## Évolution des variables

- De nombreux éléments de salaire ont été ajouté sous forme de variables de la
    population simulée :

    - `PRIMES` : primes mises à jour indépendament du salaire via majPrimes,
    - `CTI`, `NBI` : complément de traitement indiciaire et nouvelle bonification
        indiciaire,
    - `TOT_COT_EMP` : total des cotisations employeur
    - `S_NET` / `TOT_COT_SAL` : salaire net et total des cotisations salariales
        (actuellement non calculé)
    - `INFO` : Variable d'information pour les types d'entrants / sortants
    - `SIREN` de l'établissement employeur

- Changement des flags des sortants :

    - Le flag `IS_SORTANT = TRUE` l'année de la sortie des agents où ils sont
        encore en poste
    - Ils restent avec le `STATUT = FPH` et `STATUT_CONTRAT` inchangé
    - La raison de la sortie est indiquée dans la variable `INFO`: `Decede`,
      `Retraite` ou `Sortant`
    - Les variables `EQTP` et `S_BRUT`, ..., peuvent être diminués selon un
      modèle
    - L'année suivant leur sortie, où ils ne sont plus présents, les variables
      concernant leur statut sont modifiées : `STATUT = OUT` et
      `STATUT_CONTRAT = OUT` , ...

## Mise à jour du schéma général de simulation

Étant donné que les charges sont séparées du calcul du coût employeur, 
variable `TOT_COT_EMP`, la mise à jour des charges est séparée du calcul du 
coût employeur et réalisée une fois l'ensemble de la population mise à jour, 
en dernière étape de la boucle de simulation.

<img src="../reference/figures/mise_a_jour_population.jpg"/>

## Génération d'une population fictive

La fonction `genererFakePopInit` permet de générer une population fictive
mais néanmoins basée sur des caractéristiques statistiques réalistes. 

Cela permet de faire fonctionner le simulateur SMASH. Actuellement les fichiers 
des modèles ne sont pas inclus dans les sources, ce permet uniquement de faire 
tourner des versions simplifiées et non réaliste du simulateur. Un des souhaits
pour les prochaines versions est de rendre accessible les modèles. 


# 👶 SMASH 0.5.0

## Développement des fonctions de micro-simulation

- Les principaux blocs de mise à jour sont implémentés et en test

    - Évolution des grades suivant les changements de réglementation /
      nomenclature
    - Évolution des indices majorés suivant les grilles indiciaires
    - Tirage des sortants selon différents taux observés
    - Tirage des morts selon les taux de mortalité de l'INSEE
    - Tirage des retraités selon les taux de départ observés au-delà de 56
      ans.

- Différents scénarios de simulation pour tests/développement dans le dossier
  `demo`

# 🦠 SMASH 0.0

- Toute première version de SMASH, non fonctionnelle, code organisé sous forme
  de package R
